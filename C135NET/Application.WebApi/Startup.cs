﻿
using Application.Database;
using Application.Database.EntityFrameworkCore;
using Application.Services;
using Application.WebApi.Infrastructure.Configurations;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Infrastructure.Core.Timing;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Application.WebApi
{
    public class Startup
    {
        public IContainer ApplicationContainer { get; private set; }

        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            Clock.Provider = new UtcClockProvider();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddYamatoReportServices();
            services.AddGzipCompression();
            services.AddCorsConfiguration();
            MvcConfiguration.AddMvc(services);
            services.AddHealthCheckConfiguration();
            services.AddJwtAuthentication(Configuration);

            services.AddDbContext<YamatoDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")))
                .AddEntityFrameworkSqlServer();
            services.AddTransient<IDbConnection, SqlConnection>(_ => new SqlConnection(Configuration.GetConnectionString("DefaultConnection")));

            services.AddMemoryCache(options => options.ExpirationScanFrequency = TimeSpan.FromMinutes(5));

            if (Configuration.GetValue<bool>("YamatoConfig:EnableSwagger"))
            {
                services.AddSwaggerDocumentation();
            }

            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = new ApiVersion(1, 0);
                options.ApiVersionReader = new HeaderApiVersionReader("api-version");
            });

            // Create the container builder.
            var builder = new ContainerBuilder();
            builder.Populate(services);
            builder.RegisterModule<InitDbModule>();
            builder.RegisterModule<InitServiceModule>();

            ApplicationContainer = builder.Build();

            return new AutofacServiceProvider(ApplicationContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IApplicationLifetime applicationLifetime, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseGlobalExceptionHandler();
            app.UseGzipCompression();
            app.UseCorsConfiguration();
            MvcConfiguration.UseMvc(app);
            app.UseHealthCheckConfiguration();

            if (Configuration.GetValue<bool>("YamatoConfig:EnableSwagger"))
            {
                app.UseSwaggerDocumentation();
            }
        }
    }
}
