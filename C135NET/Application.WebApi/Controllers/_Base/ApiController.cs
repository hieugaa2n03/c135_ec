﻿using Microsoft.AspNetCore.Mvc;

namespace Application.WebApi.Controllers._Base
{
    [Route("api/v{version}/[controller]")]
    [ApiController]
    public class ApiController : Controller
    {
        
    }
}