﻿using Application.Services.Abstractions;
using Application.Services.Dtos;
using Application.Services.Dtos.Driver;
using Application.WebApi.Controllers._Base;
using Infrastructure.Core.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Application.WebApi.Controllers
{
    [ApiVersion("1.0")]
    public class DriverController : ApiController
    {
        private readonly IDriverService _driverService;

        public DriverController(IDriverService driverService)
        {
            _driverService = driverService;
        }

        [HttpGet]
        [Route("List-Driver")]
        public List<DriversOutput> ListDriver()
        {
            var result = _driverService.ListDriver();

            return result;
        }

        [HttpPost]
        [Route("Search-Driver")]
        public PagedResultDto<DriverOutput> SearchDriver(SearchDriverInput requestDto)
        {
            var result = _driverService.SearchDriver(requestDto);

            return result;
        }

        [HttpPost]
        [Route("Create-Driver")]
        public bool CreateDriver(DriverChangeInput requestDto)
        {
            _driverService.CreateDriver(requestDto);

            return true;
        }

        [HttpPost]
        [Route("Update-Driver")]
        public bool UpdateDriver(DriverChangeInput requestDto)
        {
            _driverService.UpdateDriver(requestDto);

            return true;
        }

        [HttpPost]
        [Route("Delete-Driver")]
        public bool DeleteDriver([FromBody] Guid driverId)
        {
            _driverService.DeleteDriver(driverId);

            return true;
        }

        //[HttpPost]
        //[Route("Save-File")]
        //public ActionResult<int> SaveFile(PathFileInput pathFileInput)
        //{
        //    var result = _driverService.SaveFile(pathFileInput);

        //    return Json(result);
        //}
    }
}