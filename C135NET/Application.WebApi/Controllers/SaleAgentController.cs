﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Services.Abstractions;
using Application.Services.Dtos.SaleAgent;
using Application.WebApi.Controllers._Base;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Application.WebApi.Controllers
{
    [ApiVersion("1.0")]
    public class SaleAgentController : ApiController
    {
        private readonly ISaleAgentService _saleAgentService;
        public SaleAgentController(ISaleAgentService saleAgentService)
        {
            _saleAgentService = saleAgentService;
        }

        [HttpGet]
        [Route("List-SaleAgent")]
        public List<SaleAgentOutput> ListSaleAgent()
        {
            return _saleAgentService.ListSaleAgent();
        }

        [HttpPost]
        [Route("Create-SaleAgent")]
        public bool CreateSaleAgent(SaleAgentOutput requestDto)
        {
            _saleAgentService.CreateSaleAgent(requestDto);
            return true;
        }
    }
}