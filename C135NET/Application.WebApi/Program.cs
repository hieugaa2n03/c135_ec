﻿using Application.Database.EntityFrameworkCore;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace Application.WebApi
{
    public class Program
    {
        public static Task Main(string[] args)
        {
            var host = CreateWebHost(args);

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var context = services.GetRequiredService<YamatoDbContext>();
                context.Database.Migrate();
            }

            return host.RunAsync();
        }

        private static IWebHost CreateWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .UseKestrel(options => { options.AddServerHeader = false; })
                //.ConfigureLogging((context, builder) =>
                //{
                //    var loggingOptions = new OptionsWrapper<LoggingOptions>(GetLoggingOptions(context.Configuration));

                //    builder.AddConsole();
                //    builder.AddDebug();
                //    builder.AddSerilog(LoggerConfig.Config(loggingOptions));
                //})
                .UseStartup<Startup>()
                .Build();
        }
    }
}
