﻿using System;
using Application.WebApi.Infrastructure.Extensions;
using Infrastructure.Core.Exceptions;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Application.WebApi.Infrastructure.Attributes
{
    [AttributeUsage(AttributeTargets.Method, Inherited = false)]
    public class ValidateModelStateAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.ModelState.IsValid)
            {
                return;
            }

            throw new ValidationException(context.ModelState.GetErrors());
        }
    }
}