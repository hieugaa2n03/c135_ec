using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Serilog;

namespace Application.WebApi.Infrastructure.Configurations
{
    public static class GlobalExceptionHandlerConfiguration
    {
        public static IApplicationBuilder UseGlobalExceptionHandler(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(new ExceptionHandlerOptions
            {
                ExceptionHandler = async context =>
                {
                    var exceptionHandler = context.Features.Get<IExceptionHandlerFeature>();
                    if (exceptionHandler != null)
                    {
                        Log.Error(exceptionHandler.Error, exceptionHandler.Error.Message);
                        await context.Response.WriteAsync(exceptionHandler.Error.Message).ConfigureAwait(false);
                    }
                }
            });

            return app;
        }
    }
}
