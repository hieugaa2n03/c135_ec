﻿using Application.Database.Entities;
using Application.Database.Repositories;
using Application.Database.UnitOfWork;
using Application.Services.Abstractions;
using Application.Services.Dtos;
using Application.Services.Dtos.Driver;
using Infrastructure.Core.Exceptions;
using Infrastructure.Core.Extensions;
using Infrastructure.Core.Models;
using Infrastructure.Core.SaveFile;
using Infrastructure.Core.Timing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Application.Services
{
    public class DriverService : IDriverService
    {
        private readonly IRepository<Driver> _driverRepository;
        private readonly IUnitOfWork _unitOfWork;

        public DriverService(IRepository<Driver> driverRepository,IUnitOfWork unitOfWork)
        {
            _driverRepository = driverRepository;
            _unitOfWork = unitOfWork;
        }

        public List<DriversOutput> ListDriver()
        {
            var result = _driverRepository.GetAll().Select(row => new DriversOutput { Id = row.Id, DriverName = row.DriverName });

            return result.ToList();
        }

        public PagedResultDto<DriverOutput> SearchDriver(SearchDriverInput requestDto)
        {
            var result = SearchDriverQuery(requestDto);
            var totalCount = result.Count();

            result = result.ApplySorting(requestDto);
            result = result.ApplyPaging(requestDto);

            var entities = result.ToList();

            return new PagedResultDto<DriverOutput>(totalCount, entities);
        }

        public void CreateDriver(DriverChangeInput requestDto)
        {
            if (requestDto.DriverName.IsEmptyString())
            {
                throw new EntityNotFoundException("Driver name not null or empty");
            }

            var driverQuery = _driverRepository.GetAll();

            var driver = new Driver
            {
                DriverName = requestDto.DriverName,
                DriverCode = requestDto.DriverCode,
                DriverContactTel = requestDto.DriverContactTel
            };

            _unitOfWork.Insert(driver);
            _unitOfWork.Commit();
        }

        public void UpdateDriver(DriverChangeInput requestDto)
        {
            var driver = _driverRepository.Get(row => row.Id.Equals(requestDto.Id));

            if (driver == null)
            {
                throw new EntityNotFoundException("Driver not exists");
            }

            if (requestDto.DriverName.IsEmptyString())
            {
                throw new EntityNotFoundException("Driver name not null or empty");
            }

            driver.DriverName = requestDto.DriverName;
            driver.DriverContactTel = requestDto.DriverContactTel;

            _unitOfWork.Update(driver);
            _unitOfWork.Commit();
        }

        public void DeleteDriver(Guid driverId)
        {
            var driver = _driverRepository.Get(row => row.Id.Equals(driverId));

            if (driver == null)
            {
                throw new EntityNotFoundException("Driver not exists");
            }

            _unitOfWork.Delete(driver);
            _unitOfWork.Commit();

        }

        //public int SaveFile(PathFileInput pathFileInput)
        //{
        //    try
        //    {
        //        PathFile pathFileTarget = new PathFile();

        //        string fileName = Path.GetFileName(pathFileInput.PathFileInputName);
        //        string newFileName = Clock.Now.ToString().Replace(@"/", "").Replace(" ", "").Replace(":", "") + fileName;
        //        string sourcePath = pathFileInput.PathFileInputName.Replace(Path.GetFileName("." + pathFileInput.PathFileInputName), "");
        //        string targetPath = pathFileTarget.pathFolder;

        //        string sourceFile = Path.Combine(sourcePath, fileName);
        //        string destFile = Path.Combine(targetPath, newFileName);

        //        if (!Directory.Exists(targetPath))
        //        {
        //            Directory.CreateDirectory(targetPath);
        //        }

        //        var fileSave = new Database.Entities.File
        //        {
        //            FileName = newFileName,
        //            FilePath = destFile,
        //            CustomerId = pathFileInput.CustomerId
        //        };

        //        _unitOfWork.Insert(fileSave);
        //        _unitOfWork.Commit();

        //        System.IO.File.Copy(sourceFile, destFile, true);
        //        return 1;
        //    }
        //    catch
        //    {
        //        return 0;
        //    }
        //}

        private IQueryable<DriverOutput> SearchDriverQuery(SearchDriverInput requestDto)
        {
            var query = _driverRepository.GetAll().Select(row => new DriverOutput
            {
                Id = row.Id,
                DriverName = row.DriverName,
                DriverCode = row.DriverCode,
                DriverContactTel = row.DriverContactTel
            });

            query = query.WhereIf(!string.IsNullOrEmpty(requestDto.DriverCode), row => row.DriverCode.ToLower().Contains(requestDto.DriverCode.ToLower()));

            query = query.WhereIf(!string.IsNullOrEmpty(requestDto.DriverName), row => row.DriverName.ToLower().Contains(requestDto.DriverName.ToLower()));

            query = query.WhereIf(!string.IsNullOrEmpty(requestDto.DriverContactTel), row => row.DriverContactTel.ToLower().Contains(requestDto.DriverContactTel.ToLower()));

            return query;
        }
    }
}
