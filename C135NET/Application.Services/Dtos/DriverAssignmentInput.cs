﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Application.Services.Dtos
{
    public class DriverAssignmentInput
    {
        public Guid BillId { get; set; }

        public string DriverName { get; set; }

        public string DeviceCode { get; set; }

        [MaxLength(255)]
        public string VehicleType { get; set; }

        [MaxLength(255)]
        public string PlateNo { get; set; }
    }
}
