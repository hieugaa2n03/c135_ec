﻿

using System;

namespace Application.Services.Dtos.Driver
{
    public class DriversOutput
    {
        public Guid Id { get; set; }

        public string DriverName { get; set; }
    }
}
