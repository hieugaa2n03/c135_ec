﻿using Infrastructure.Core.Models;

namespace Application.Services.Dtos
{
    public class SearchDriverInput : IPagedAndSortedResultRequest
    {
        public string DriverCode { get; set; }

        public string DriverName { get; set; }

        public string DriverContactTel { get; set; }

        public int SkipCount { get; set; }

        public int MaxResultCount { get; set; }

        public string Sorting { get; set; }
    }
}
