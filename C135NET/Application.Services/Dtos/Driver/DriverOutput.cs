﻿using System;

namespace Application.Services.Dtos
{
    public class DriverOutput
    {
        public Guid Id { get; set; }

        public string DriverCode { get; set; }

        public string DriverName { get; set; }

        public string DriverContactTel { get; set; }
    }
}
