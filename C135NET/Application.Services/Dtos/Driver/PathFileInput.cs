﻿using System;

namespace Application.Services.Dtos.Driver
{
    public class PathFileInput
    {
        public string PathFileInputName { get; set; }

        public Guid CustomerId { get; set; }
    }
}
