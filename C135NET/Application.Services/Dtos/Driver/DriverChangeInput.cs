﻿using System;

namespace Application.Services.Dtos.Driver
{
    public class DriverChangeInput
    {
        public Guid? Id { get; set; }

        public string DriverName { get; set; }

        public string DriverCode { get; set; }

        public string DriverContactTel { get; set; }
    }
}
