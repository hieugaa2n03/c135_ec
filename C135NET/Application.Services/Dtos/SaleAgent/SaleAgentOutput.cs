﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Services.Dtos.SaleAgent
{
    public class SaleAgentOutput
    {
        public Guid? Id { get; set; }

        public string SaleAgentName { get; set; }
    }
}