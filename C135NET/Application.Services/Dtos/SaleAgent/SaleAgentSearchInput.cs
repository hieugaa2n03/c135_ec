﻿using Infrastructure.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Services.Dtos.SaleAgent
{
    public class SaleAgentSearchInput: IPagedAndSortedResultRequest
    {

        public string SaleAgentName { get; set; }

        public int SkipCount { get; set; }

        public int MaxResultCount { get; set; }

        public string Sorting { get; set; }
    }
}
