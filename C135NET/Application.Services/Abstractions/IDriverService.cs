﻿using Application.Services.Dtos;
using Application.Services.Dtos.Driver;
using Infrastructure.Core.Models;
using System;
using System.Collections.Generic;

namespace Application.Services.Abstractions
{
    public interface IDriverService : IGeneratorService
    {
        List<DriversOutput> ListDriver();

        PagedResultDto<DriverOutput> SearchDriver(SearchDriverInput driverListInput);

        void CreateDriver(DriverChangeInput driverInput);

        void UpdateDriver(DriverChangeInput driverUpdateInput);

        void DeleteDriver(Guid driverId);

        //int SaveFile(PathFileInput pathFileInput);
    }
}
