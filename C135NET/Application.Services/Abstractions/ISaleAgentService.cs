﻿using Application.Services.Dtos.SaleAgent;
using Infrastructure.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Services.Abstractions
{
    public interface ISaleAgentService : IGeneratorService
    {
        List<SaleAgentOutput> ListSaleAgent();

        //PagedResultDto<SaleAgentOutput> SearchSaleAgent(SaleAgentSearchInput saleAgentListInput);

        void CreateSaleAgent(SaleAgentOutput saleAgentInput);

        void UpdateSaleAgent(SaleAgentOutput saleAgnetUpdateInput);

        void DeleteSaleAgent(Guid saleAgnetId);
    }
}
    