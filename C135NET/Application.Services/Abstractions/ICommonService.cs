﻿using Application.Database.Entities;
using Infrastructure.Core.Enumerations;
using System;
using System.Linq;

namespace Application.Services.Abstractions
{
    public interface ICommonService : IGeneratorService
    {
        string GetDriverNames(Guid billId, string driverName);

        string GetDriverNames(Guid billId, Guid deviceId, Guid vehicleId, Guid temperatureId, string driverName);

        string GetRecipientAddress(Guid RecipientCustomerId);

        //Temperature GetTemperature(float temp1, float temp2, float temp3, float temp4, float temp5);

        //Service GetService(bool storage, bool stevedoring, float cod, string others);

        //Package GetPackage(int numberOfPackage, float weight, ParcelTypeEnum parcelType);
    }
}
