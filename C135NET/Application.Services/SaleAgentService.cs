﻿using Application.Database.Entities;
using Application.Database.Repositories;
using Application.Database.UnitOfWork;
using Application.Services.Abstractions;
using Application.Services.Dtos.SaleAgent;
using Infrastructure.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infrastructure.Core.Extensions;
using Infrastructure.Core.Exceptions;

namespace Application.Services
{
    public class SaleAgentService: ISaleAgentService
    {
        private readonly IRepository<SaleAgent> _saleAgentRepository;
        private readonly IUnitOfWork _unitOfWork;

        public SaleAgentService(IRepository<SaleAgent> saleAgentRepository, IUnitOfWork unitOfWork)
        {
            _saleAgentRepository = saleAgentRepository;
            _unitOfWork = unitOfWork;
        }

        public List<SaleAgentOutput> ListSaleAgent()
        {
            var result = _saleAgentRepository.GetAll().Select(row => new SaleAgentOutput { Id = row.Id, SaleAgentName = row.SaleAgentName});
            return result.ToList();
        }

        //public PagedResultDto<SaleAgentOutput> SearchSaleAgent(SaleAgentSearchInput requestDto)
        //{

        //    return 1;
        //}

        public void CreateSaleAgent(SaleAgentOutput requestDto)
        {
            if(requestDto.SaleAgentName.IsEmptyString())
            {
                throw new EntityNotFoundException("SaleAgent name not null or empty");
            }
            var saleAgentQUery = _saleAgentRepository.GetAll();

            var saleAgent = new SaleAgent()
            {
                SaleAgentName = requestDto.SaleAgentName,
            };
            _unitOfWork.Insert(saleAgent);
            _unitOfWork.Commit();
        }

        public void UpdateSaleAgent(SaleAgentOutput requestDto)
        {

        }

        public void DeleteSaleAgent(Guid saleAgentId)
        {

        }
    }
}
