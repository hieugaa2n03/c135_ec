﻿using Application.Services.Abstractions;
using Autofac;

namespace Application.Services
{
    public class InitServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Business services
            builder.RegisterAssemblyTypes(GetType().Assembly)
                .Where(type => typeof(IGeneratorService).IsAssignableFrom(type))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }

    }
}