﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Infrastructure.Core.Models
{
    [Serializable]
    public class PagedResultRequestDto : LimitedResultRequestDto, IPagedResultRequest
    {
        [Range(0, int.MaxValue)]
        public virtual int SkipCount { get; set; }
    }
}
