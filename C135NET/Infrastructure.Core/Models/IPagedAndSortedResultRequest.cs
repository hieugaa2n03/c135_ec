﻿namespace Infrastructure.Core.Models
{
    public interface IPagedAndSortedResultRequest : IPagedResultRequest, ISortedResultRequest
    {
    }
}
