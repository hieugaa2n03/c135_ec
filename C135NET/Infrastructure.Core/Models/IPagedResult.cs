﻿namespace Infrastructure.Core.Models
{
    public interface IPagedResult<T> : IListResult<T>, IHasTotalCount
    {
    }
}
