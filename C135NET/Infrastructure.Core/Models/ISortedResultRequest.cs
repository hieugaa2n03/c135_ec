﻿namespace Infrastructure.Core.Models
{
    public interface ISortedResultRequest
    {
        string Sorting { get; set; }
    }
}
