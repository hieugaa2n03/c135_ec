﻿namespace Infrastructure.Core.Models
{
    public class KeyValue
    {
        public dynamic Key { get; set; }

        public dynamic Value { get; set; }
    }
}
