﻿using System.Reflection;

namespace Infrastructure.Core.SaveFile
{
    public class PathFile
    {
        public string pathFolder =  Assembly.GetExecutingAssembly().Location.Replace("Infrastructure.Core.dll","FolderSavePDF");
    }
}
