﻿namespace Infrastructure.Core.Constants
{
    public class ExcelHeaderConstant
    {
        public static readonly string[] ProductHeaders =
        {
            "Code",
            "Name",
            "Description"
        };
    }
}
