﻿namespace Infrastructure.Core.Enumerations
{
    public enum BillTypeEnum
    {
        Pickup = 10,
        Delivery = 20,
        Direct = 30,
        Others = 40,
        Storage = 50
    }
}
