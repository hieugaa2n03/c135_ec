﻿namespace Infrastructure.Core.Enumerations
{
    public enum BillStatusEnum
    {
        Open = 10,
        ToStorage = 20,
        Delivery = 30,
        Finish = 40,
        FailToStorage = 50,
        FailToCustomer = 60,
        Storage = 70,
        BreackPackage = 80
    }
}
