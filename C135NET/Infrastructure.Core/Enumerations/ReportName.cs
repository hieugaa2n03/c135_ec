﻿namespace Infrastructure.Core.Enumerations
{
    public enum ReportName
    {
        MonthlyTemperature = 10,
        YearlyRevenueAgent = 20,
        YearlyRevenueCustomer = 30,
        CustomerBillDelivery = 40,
    }
}
