﻿namespace Infrastructure.Core.Enumerations
{
    public enum ParcelTypeEnum
    {
        Normal = 10,
        Frozen = 20,
        Chilled = 30
    }
}
