using System;
using System.Linq;
using System.Linq.Expressions;
using System.Linq.Dynamic.Core;
using Infrastructure.Core.Models;
using JetBrains.Annotations;

namespace Infrastructure.Core.Extensions
{
    public static class QueryableExtensions
    {
        public static IQueryable<T> PageBy<T>( this IQueryable<T> query, int skipCount, int maxResultCount)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            return query.Skip(skipCount).Take(maxResultCount);
        }

        public static IQueryable<T> PageBy<T>( this IQueryable<T> query, IPagedResultRequest pagedResultRequest)
            => query.PageBy(pagedResultRequest.SkipCount, pagedResultRequest.MaxResultCount);

        public static IQueryable<T> WhereIf<T>( this IQueryable<T> query, bool condition, Expression<Func<T, bool>> predicate)
            => condition ? query.Where(predicate) : query;

        public static IQueryable<T> WhereIf<T>( this IQueryable<T> query, bool condition, Expression<Func<T, int, bool>> predicate)
            => condition ? query.Where(predicate) : query;

        public static IQueryable<TResult> LeftJoin<TSource, TInner, TKey, TResult>(this IQueryable<TSource> sources, IQueryable<TInner> inner, Func<TSource, TKey> pk, Func<TInner, TKey> fk, Func<TSource, TInner, TResult> result) where TSource : class where TInner : class
           => from s in sources join i in inner on pk(s) equals fk(i) into joinData from left in joinData.DefaultIfEmpty() select result(s, left);

        public static IQueryable<TResult> RightJoin<TSource, TInner, TKey, TResult>(this IQueryable<TSource> sources, IQueryable<TInner> inner, Func<TSource, TKey> pk, Func<TInner, TKey> fk, Func<TSource, TInner, TResult> result) where TSource : class where TInner : class
            => from i in inner join s in sources on fk(i) equals pk(s) into joinData from right in joinData.DefaultIfEmpty() select result(right, i);

        public static IQueryable<TResult> FullJoin<TSource, TInner, TKey, TResult>(this IQueryable<TSource> sources, IQueryable<TInner> inner, Func<TSource, TKey> pk, Func<TInner, TKey> fk, Func<TSource, TInner, TResult> result) where TSource : class where TInner : class
            => sources.LeftJoin(inner, pk, fk, result).Union(sources.RightJoin(inner, pk, fk, result));

        public static IQueryable<TResult> LeftExcludingJoin<TSource, TInner, TKey, TResult>(this IQueryable<TSource> sources, IQueryable<TInner> inner, Func<TSource, TKey> pk, Func<TInner, TKey> fk, Func<TSource, TInner, TResult> result) where TSource : class where TInner : class
            => from s in sources join i in inner on pk(s) equals fk(i) into joinData from left in joinData.DefaultIfEmpty() where left == null select result(s, left);

        public static IQueryable<TResult> RightExcludingJoin<TSource, TInner, TKey, TResult>(this IQueryable<TSource> sources, IQueryable<TInner> inner, Func<TSource, TKey> pk, Func<TInner, TKey> fk, Func<TSource, TInner, TResult> result) where TSource : class where TInner : class
            => from i in inner join s in sources on fk(i) equals pk(s) into joinData from right in joinData.DefaultIfEmpty() where right == null select result(right, i);

        public static IQueryable<TResult> FullExcludingJoin<TSource, TInner, TKey, TResult>(this IQueryable<TSource> sources, IQueryable<TInner> inner, Func<TSource, TKey> pk, Func<TInner, TKey> fk, Func<TSource, TInner, TResult> result) where TSource : class where TInner : class
            => sources.LeftExcludingJoin(inner, pk, fk, result).Union(sources.RightExcludingJoin(inner, pk, fk, result));

        public static IQueryable<TEntity> ApplySorting<TEntity, TInput>( this IQueryable<TEntity> query, TInput input,
            string defaultSortCriteria = "Id DESCENDING") where TEntity : class
        {
            switch (input)
            {
                // Try to sort query if available
                case ISortedResultRequest sortInput:
                    return query.OrderBy(!sortInput.Sorting.IsNullOrWhiteSpace() ? sortInput.Sorting : defaultSortCriteria);
                // IQueryable.Take requires sorting, so we should sort if Take will be used.
                case ILimitedResultRequest _:
                    // This will throw exception if there is no Id column in entity class
                    // This will happened with PiManagement table because this table doesn't have Id column
                    return query.OrderBy(defaultSortCriteria);
                default:
                    // No sorting
                    return query;
            }
        }

        public static IQueryable<TEntity> ApplyPaging<TEntity, TInput>( this IQueryable<TEntity> query, TInput input)
            where TEntity : class
        {
            switch (input)
            {
                // Try to use paging if available
                case IPagedResultRequest pagedInput:
                    return query.PageBy(pagedInput);
                // Try to limit query result if available
                case ILimitedResultRequest limitedInput:
                    return query.Take(limitedInput.MaxResultCount);
                default:
                    // No paging
                    return query;
            }
        }
    }
}
