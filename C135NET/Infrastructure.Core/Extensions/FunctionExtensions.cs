﻿using Infrastructure.Core.Exceptions;
using Infrastructure.Core.Models;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Core.Extensions
{
    public static class FunctionExtensions
    {
        public static dynamic GetValueByKey(this List<KeyValue> keyValues, dynamic key)
        {
            foreach (var item in keyValues)
            {
                if (item.Key == key)
                {
                    return item.Value;
                }
            }

            return new EntityNotFoundException("Value not fould");
        }

        public static string GetValueMax(this IQueryable<dynamic> tableQuery, string nameOfValue, string key)
        {
            if (!tableQuery.Any() && key == null)
            {
                return "00000001";
            }
            else if (tableQuery.Any() && key == null)
            {
                int number = 0;

                foreach (var item in tableQuery)
                {
                    var code = (dynamic)item.GetType().GetProperty(nameOfValue).GetValue(item, null);

                    var value = GetNumber(code);

                    if (value > number)
                    {
                        number = value;
                    }
                }

                return (number + 1).ToString("00000000");
            }
            else
            {
                var value = GetNumber(key);

                return (value + 1).ToString("00000000");
            }
        }

        public static int GetNumber(this string value)
        {
            int result = 0;
            bool success = int.TryParse(new string(value
                                 .SkipWhile(x => !char.IsDigit(x))
                                 .TakeWhile(x => char.IsDigit(x))
                                 .ToArray()), out result);
            return result;
        }
    }
}
