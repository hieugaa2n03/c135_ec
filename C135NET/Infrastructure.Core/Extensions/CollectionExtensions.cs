using JetBrains.Annotations;
using System;
using System.Collections.Generic;

namespace Infrastructure.Core.Extensions
{
    public static class CollectionExtensions
    {
        public static bool IsNullOrEmpty<T>(this ICollection<T> source)
        {
            return source == null || source.Count <= 0;
        }

        public static bool AddIfNotContains<T>([NotNull] this ICollection<T> source, [NotNull] T item)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (source.Contains(item))
            {
                return false;
            }

            source.Add(item);
            return true;
        }

        public static void ForEach<T>([NotNull]this IEnumerable<T> items, Action<T> action)
        {
            foreach (var obj in items)
            {
                action(obj);
            }
        }

        public static void InsertAtFirstPlace<T>([NotNull] this List<T> source, [NotNull] T item)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            source.Insert(0, item);
        }
    }
}
