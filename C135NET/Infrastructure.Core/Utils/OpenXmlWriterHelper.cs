﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Collections.Generic;

namespace Infrastructure.Core.Utils
{
    public class OpenXmlWriterHelper
    {
        /// <summary>
        /// Contains the shared string as the key, and the index as the value. index is 0 base
        /// </summary>
        private readonly Dictionary<string, int> _shareStringDictionary = new Dictionary<string, int>();

        private int _shareStringMaxIndex;

        /// <summary>
        /// Write out the share string xml. Call this after writing out all shared string values in sheet
        /// </summary>
        /// <param name="workbookPart">Excel WorkbookPart</param>
        public void CreateShareStringPart(WorkbookPart workbookPart)
        {
            if (_shareStringMaxIndex <= 0)
            {
                return;
            }

            var sharedStringPart = workbookPart.AddNewPart<SharedStringTablePart>();
            using (var writer = OpenXmlWriter.Create(sharedStringPart))
            {
                writer.WriteStartElement(new SharedStringTable());
                foreach (var item in _shareStringDictionary)
                {
                    writer.WriteStartElement(new SharedStringItem());
                    writer.WriteElement(new Text(item.Key));
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// CellValues = Boolean -> expects cellValue "True" or "False"
        /// CellValues = InlineString -> stores string within sheet
        /// CellValues = SharedString -> stores index within sheet. If this is called, please call CreateShareStringPart after creating all sheet data to create the shared string part
        /// CellValues = Date -> expects ((DateTime)value).ToOADate().ToString(CultureInfo.InvariantCulture) as cellValue 
        ///              and new OpenXmlAttribute[] { new OpenXmlAttribute("s", null, "1") }.ToList() as attributes so that the correct formatting can be applied
        /// </summary>
        /// <param name="writer">OpenXmlWriter</param>
        /// <param name="cellValue">cellValue</param>
        /// <param name="dataType">dataType</param>
        /// <param name="attributes">attributes</param>
        public void WriteCellValueSax(
            OpenXmlWriter writer,
            string cellValue,
            CellValues dataType,
            List<OpenXmlAttribute> attributes = null)
        {
            var checkAttributes = attributes;
            switch (dataType)
            {
                case CellValues.InlineString:
                    {
                        if (checkAttributes == null)
                        {
                            checkAttributes = new List<OpenXmlAttribute>();
                        }
                        checkAttributes.Add(new OpenXmlAttribute("t", null, "inlineStr"));
                        writer.WriteStartElement(new Cell(), checkAttributes);
                        writer.WriteElement(new InlineString(new Text(cellValue)));
                        writer.WriteEndElement();
                        break;
                    }

                case CellValues.SharedString:
                    {
                        if (string.IsNullOrEmpty(cellValue))
                        {
                            cellValue = string.Empty;
                        }
                        if (checkAttributes == null)
                        {
                            checkAttributes = new List<OpenXmlAttribute>();
                        }
                        checkAttributes.Add(new OpenXmlAttribute("t", null, "s"));//shared string type
                        writer.WriteStartElement(new Cell(), checkAttributes);
                        if (!_shareStringDictionary.ContainsKey(cellValue))
                        {
                            _shareStringDictionary.Add(cellValue, _shareStringMaxIndex);
                            _shareStringMaxIndex++;
                        }

                        //writing the index as the cell value
                        writer.WriteElement(new CellValue(_shareStringDictionary[cellValue].ToString()));


                        writer.WriteEndElement();//cell

                        break;
                    }

                case CellValues.Date:
                    {
                        if (checkAttributes == null)
                        {
                            writer.WriteStartElement(new Cell { DataType = CellValues.Number });
                        }
                        else
                        {
                            writer.WriteStartElement(new Cell { DataType = CellValues.Number }, checkAttributes);
                        }

                        writer.WriteElement(new CellValue(cellValue));

                        writer.WriteEndElement();

                        break;
                    }

                case CellValues.Boolean:
                    {
                        if (checkAttributes == null)
                        {
                            checkAttributes = new List<OpenXmlAttribute>();
                        }
                        checkAttributes.Add(new OpenXmlAttribute("t", null, "b"));//boolean type
                        writer.WriteStartElement(new Cell(), checkAttributes);
                        writer.WriteElement(new CellValue(cellValue == "True" ? "1" : "0"));
                        writer.WriteEndElement();
                        break;
                    }

                default:
                    {
                        if (checkAttributes == null)
                        {
                            writer.WriteStartElement(new Cell { DataType = dataType });
                        }
                        else
                        {
                            writer.WriteStartElement(new Cell { DataType = dataType }, checkAttributes);
                        }
                        writer.WriteElement(new CellValue(cellValue));

                        writer.WriteEndElement();


                        break;
                    }
            }
        }
    }
}
