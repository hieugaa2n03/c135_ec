﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace Infrastructure.Core.Utils
{
    public static class StyleSheetHelper
    {
        public static Alignment alignmentCenter =
            ExcelUtils.GenerateAlignment(
                true,
                VerticalAlignmentValues.Center,
                HorizontalAlignmentValues.Center
                );

        public static void CreateReportStyleSheet(WorkbookPart workbookPart)
        {
            var stylesheet = CreateDefaultStylesheet();

            var borders = stylesheet.Borders;
            borders.AppendChild(ExcelUtils.GenerateBorder(BorderStyleValues.Thin, 1, 1, 1, 1));
            borders.AppendChild(ExcelUtils.GenerateBorder(BorderStyleValues.Thin, 0, 0, 0, 1));
            borders.Count = (uint)borders.ChildElements.Count;

            var fonts = stylesheet.Fonts;
            fonts.AppendChild(ExcelUtils.GenerateFont("Segoe UI", 8)); // 1
            fonts.AppendChild(ExcelUtils.GenerateFont("Segoe UI", 10, true)); // 2
            fonts.AppendChild(ExcelUtils.GenerateFont("Calibri", 11)); // 3
            fonts.AppendChild(ExcelUtils.GenerateFont("Meiryo UI", 14)); // 3
            fonts.Count = (uint)fonts.ChildElements.Count;

            var fills = stylesheet.Fills;
            fills.AppendChild(ExcelUtils.GenerateFill("FFFF00")); // 2
            fills.AppendChild(ExcelUtils.GenerateFill("FF0000")); // 3

            fills.Count = (uint)fills.ChildElements.Count;

            var numberingFormats = stylesheet.NumberingFormats;
            //number less than 164 is reserved by excel for default formats
            uint iExcelIndex = 165;
            var numberingFormat = new NumberingFormat
            {
                NumberFormatId = iExcelIndex++,
                FormatCode = "dd/mm/yyyy hh:mm:ss"
            };
            numberingFormats.Append(numberingFormat);

            var numberingFormat1 = new NumberingFormat
            {
                NumberFormatId = iExcelIndex++,
                FormatCode = "dd/mm/yyyy"
            };
            numberingFormats.Append(numberingFormat1);

            numberingFormats.Count = (uint)numberingFormats.ChildElements.Count;

            //this should already contain a default StyleIndex of 0
            var cellFormats = stylesheet.CellFormats;

            var cellFormat = new CellFormat
            {
                FontId = 1,
                BorderId = 1,
                Alignment = ExcelUtils.GenerateAlignment(true, VerticalAlignmentValues.Bottom, HorizontalAlignmentValues.Left),
                ApplyAlignment = true,
            }; // Row data format is defined as StyleIndex = 1
            cellFormats.Append(cellFormat);

            var cellFormat1 = new CellFormat
            {
                FontId = 2,
                BorderId = 1,
                ApplyBorder = true,
                Alignment = ExcelUtils.GenerateAlignment(true, VerticalAlignmentValues.Bottom, HorizontalAlignmentValues.Center),
                ApplyAlignment = true,
                FillId = 2,
                ApplyFill = true,
            }; // Header format is defined as StyleIndex = 2
            cellFormats.Append(cellFormat1);

            var cellFormat2 = new CellFormat
            {
                FontId = 1,
                BorderId = 1,
                Alignment = ExcelUtils.GenerateAlignment(true, VerticalAlignmentValues.Bottom, HorizontalAlignmentValues.Left),
                ApplyAlignment = true,
                ApplyNumberFormat = true,
                NumberFormatId = numberingFormat.NumberFormatId
            }; // Row data format is defined as StyleIndex = 3
            cellFormats.Append(cellFormat2);

            var cellFormat3 = new CellFormat
            {
                FontId = 1,
                BorderId = 1,
                Alignment = ExcelUtils.GenerateAlignment(true, VerticalAlignmentValues.Bottom, HorizontalAlignmentValues.Left),
                ApplyAlignment = true,
                ApplyNumberFormat = true,
                NumberFormatId = numberingFormat1.NumberFormatId
            }; // Row data format is defined as StyleIndex = 4
            cellFormats.Append(cellFormat3);

            var cellFormat4 = new CellFormat
            {
                FontId = 2,
                BorderId = 1,
                ApplyBorder = true,
                Alignment = ExcelUtils.GenerateAlignment(true, VerticalAlignmentValues.Bottom, HorizontalAlignmentValues.Center),
                ApplyAlignment = true,
                FillId = 3,
                ApplyFill = true,
            }; // Header format is defined as StyleIndex = 5

            cellFormats.Count = (uint)cellFormats.ChildElements.Count;

            var style = workbookPart.WorkbookStylesPart.Stylesheet = stylesheet;
            style.Save();
        }

        private static Stylesheet CreateDefaultStylesheet()
        {
            var ss = new Stylesheet();

            var fts = new Fonts();
            var ft = new Font();
            var ftn = new FontName { Val = "Arial" };
            var ftsz = new FontSize { Val = 11 };
            ft.FontName = ftn;
            ft.FontSize = ftsz;
            fts.Append(ft);
            fts.Count = (uint)fts.ChildElements.Count;

            var fills = new Fills();

            //default fills used by Excel, don't changes these

            var fill = new Fill();
            var patternFill = new PatternFill { PatternType = PatternValues.None };
            fill.PatternFill = patternFill;
            fills.AppendChild(fill);

            fill = new Fill();
            patternFill = new PatternFill { PatternType = PatternValues.Gray125 };
            fill.PatternFill = patternFill;
            fills.AppendChild(fill);

            fills.Count = (uint)fills.ChildElements.Count;

            var borders = new Borders();
            var border = new Border
            {
                LeftBorder = new LeftBorder(),
                RightBorder = new RightBorder(),
                TopBorder = new TopBorder(),
                BottomBorder = new BottomBorder(),
                DiagonalBorder = new DiagonalBorder()
            };
            borders.Append(border);
            borders.Count = (uint)borders.ChildElements.Count;

            var csfs = new CellStyleFormats();
            var cf = new CellFormat
            {
                NumberFormatId = 0,
                FontId = 0,
                FillId = 0,
                BorderId = 0,
                //Alignment = {WrapText = false}
            };
            csfs.Append(cf);
            csfs.Count = (uint)csfs.ChildElements.Count;


            var cfs = new CellFormats();

            cf = new CellFormat
            {
                NumberFormatId = 0,
                FontId = 0,
                FillId = 0,
                BorderId = 0,
                FormatId = 0,
                //Alignment = { WrapText = false }
            };
            cfs.Append(cf);

            var nfs = new NumberingFormats();

            nfs.Count = (uint)nfs.ChildElements.Count;
            cfs.Count = (uint)cfs.ChildElements.Count;

            ss.Append(nfs);
            ss.Append(fts);
            ss.Append(fills);
            ss.Append(borders);
            ss.Append(csfs);
            ss.Append(cfs);

            var css = new CellStyles(
              new CellStyle
              {
                  Name = "Normal",
                  FormatId = 0,
                  BuiltinId = 0,
              }
            );

            css.Count = (uint)css.ChildElements.Count;
            ss.Append(css);

            var dfs = new DifferentialFormats { Count = 0 };
            ss.Append(dfs);

            var tss = new TableStyles
            {
                Count = 0,
                DefaultTableStyle = "TableStyleMedium9",
                DefaultPivotStyle = "PivotStyleLight16"
            };
            ss.Append(tss);
            return ss;
        }
    }
}
