using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Infrastructure.Core.Utils
{
    public static class ExcelUtils
    {
        private static readonly Regex RegChar = new Regex("[A-Za-z]+");
        private static readonly Regex RegNum = new Regex(@"\d+");

        public static Cell InsertValue(this Cell cell, string value, bool isStringType = false)
        {
            if (string.IsNullOrEmpty(value))
            {
                return cell;
            }

            if (isStringType)
            {
                cell.DataType = CellValues.InlineString;
                var inlineString = new InlineString();
                var text = new Text
                {
                    Text = string.IsNullOrEmpty(value) ? string.Empty : value
                };

                inlineString.AppendChild(text);
                cell.AppendChild(inlineString);

                return cell;
            }

            if (int.TryParse(value, out var resInt))
            {
                var cellValue = new CellValue
                {
                    Text = resInt.ToString()
                };

                cell.AppendChild(cellValue);
            }
            else if (double.TryParse(value, out var resDouble))
            {
                var cellValue = new CellValue
                {
                    Text = resDouble.ToString(CultureInfo.InvariantCulture)
                };

                cell.AppendChild(cellValue);
            }
            else if (value.IndexOf("=", StringComparison.Ordinal) == 0)
            {
                var cellformula = new CellFormula
                {
                    Text = value.Substring(1)
                };
                cell.Append(cellformula);
            }
            else
            {
                cell.DataType = CellValues.InlineString;
                var inlineString = new InlineString();
                var text = new Text
                {
                    Text = string.IsNullOrEmpty(value) ? string.Empty : value
                };

                inlineString.AppendChild(text);
                cell.AppendChild(inlineString);
            }

            return cell;
        }

        public static Cell GetCell(this SheetData sheetData, int columnIndex, int rowIndex)
        {
            var row = sheetData.GetRow(Convert.ToUInt32(rowIndex));

            var cellName = GetColumnName(columnIndex) + rowIndex;

            if (row.Elements<Cell>().Any(c => c.CellReference.Value.Equals(cellName)))
            {
                return row.Elements<Cell>().First(c => c.CellReference.Value.Equals(cellName));
            }

            // Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
            var refCell = row.Elements<Cell>()
                .Where(cell => cell.CellReference.Value.Length == cellName.Length)
                .FirstOrDefault(cell => string.Compare(cell.CellReference.Value, cellName, StringComparison.OrdinalIgnoreCase) > 0);

            var newCell = new Cell
            {
                CellReference = cellName
            };

            row.InsertBefore(newCell, refCell);

            return newCell;
        }

        public static Row GetRow(this SheetData sheetData, uint rowIndex)
        {
            if (sheetData.Elements<Row>().Any(r => r.RowIndex == rowIndex))
            {
                return sheetData.Elements<Row>().First(r => r.RowIndex == rowIndex);
            }

            var row = new Row
            {
                RowIndex = rowIndex
            };

            sheetData.Append(row);

            return row;
        }

        public static SheetData GetSheetData(this WorkbookPart workbookPart, string sheetName)
        {
            var sheet = workbookPart.Workbook.Descendants<Sheet>().FirstOrDefault(s => s.Name == sheetName);

            if (sheet == null)
            {
                workbookPart.Workbook.Descendants<Sheet>().FirstOrDefault();
            }

            var worksheetPart = (WorksheetPart) workbookPart.GetPartById(sheet.Id);
            return worksheetPart.Worksheet.GetFirstChild<SheetData>();
        }

        public static void DeleteSheet(this WorkbookPart workbookPart, string sheetName)
        {
            //Get the SheetToDelete from workbook.xml
            var templateSheet = workbookPart.Workbook.Descendants<Sheet>().FirstOrDefault(s => s.Name == sheetName);
            if (templateSheet == null)
            {
                return;
            }

            // Remove the sheet reference from the workbook.
            var worksheetPart = (WorksheetPart)workbookPart.GetPartById(templateSheet.Id);
            templateSheet.Remove();

            // Delete the worksheet part.
            workbookPart.DeletePart(worksheetPart);
        }

        public static Cell AddAlignment(this Cell cell, WorkbookPart workbookPart, Alignment alignment)
        {
            var cellFormat = cell.GetCellFormat(workbookPart);

            cellFormat.Alignment = alignment;
            cell.StyleIndex = workbookPart.GetNewStyleIndex(cellFormat);

            return cell;
        }

        public static Cell AddFont(this Cell cell, WorkbookPart workbookPart, Font font)
        {
            var cellFormat = cell.GetCellFormat(workbookPart);

            cellFormat.FontId = workbookPart.GetFontId(font);
            cell.StyleIndex = workbookPart.GetNewStyleIndex(cellFormat);

            return cell;
        }

        public static Cell AddBorder(this Cell cell, WorkbookPart workbookPart, Border border)
        {
            var cellFormat = cell.GetCellFormat(workbookPart);

            cellFormat.BorderId = workbookPart.GetBorderId(border);
            cell.StyleIndex = workbookPart.GetNewStyleIndex(cellFormat);

            return cell;
        }

        public static Alignment GenerateAlignment(
            bool isWrapText,
            VerticalAlignmentValues verticalAlignment,
            HorizontalAlignmentValues horizontalAlignment)
        {
            return new Alignment
            {
                WrapText = isWrapText,
                Vertical = verticalAlignment,
                Horizontal = horizontalAlignment
            };
        }

        public static Fill GenerateFill(string hexBinaryValue)
        {
            var patternFill = new PatternFill()
            {
                ForegroundColor = new ForegroundColor() { Rgb = HexBinaryValue.FromString(hexBinaryValue) },
                PatternType = PatternValues.Solid
            };
            return new Fill()
            {
                PatternFill = patternFill
            };
        }

        public static Font GenerateFont(string fontName, int fontSize, bool isBold = false)
        {
            return isBold
                ? new Font(new FontSize { Val = fontSize }, new FontName { Val = fontName }, new Bold())
                : new Font(new FontSize { Val = fontSize }, new FontName { Val = fontName });
        }

        public static Border GenerateBorder(BorderStyleValues borderStyle, params int[] borders)
        {
            var border = new Border();

            var checkBorders = borders;

            var diagonalBorder = new DiagonalBorder();

            if (borders.Length == 0)
            {
                checkBorders = new[] { 1, 1, 1, 1 };
            }

            if (checkBorders.Length > 0 && checkBorders[0] == 1)
            {
                var leftBorder = new LeftBorder
                {
                    Style = borderStyle
                };

                border.Append(leftBorder);
                leftBorder.Append(new Color { Indexed = 64U });
            }

            if (checkBorders.Length > 1 && checkBorders[1] == 1)
            {
                var rightBorder = new RightBorder
                {
                    Style = borderStyle
                };

                border.Append(rightBorder);
                rightBorder.Append(new Color { Indexed = 64U });
            }
            if (checkBorders.Length > 2 && checkBorders[2] == 1)
            {
                var topBorder = new TopBorder
                {
                    Style = borderStyle
                };

                border.Append(topBorder);
                topBorder.Append(new Color { Indexed = 64U });
            }
            if (checkBorders.Length > 3 && checkBorders[3] == 1)
            {
                var bottomBorder = new BottomBorder
                {
                    Style = borderStyle
                };

                border.Append(bottomBorder);
                bottomBorder.Append(new Color { Indexed = 64U });
            }

            border.Append(diagonalBorder);

            return border;
        }

        public static uint GetFontId(this WorkbookPart workbookPart, Font font)
        {
            var fonts = workbookPart.WorkbookStylesPart.Stylesheet.Elements<Fonts>().First();
            fonts.Append(font);

            return fonts.Count++;
        }

        public static uint GetBorderId(this WorkbookPart workbookPart, Border border)
        {
            var borders = workbookPart.WorkbookStylesPart.Stylesheet.Elements<Borders>().First();
            borders.Append(border);

            return borders.Count++;
        }

        public static uint GetNewStyleIndex(this WorkbookPart workbookPart, CellFormat cellFormat)
        {
            var cellFormats = workbookPart.WorkbookStylesPart.Stylesheet.Elements<CellFormats>().First();

            cellFormats.Append(cellFormat);

            return cellFormats.Count++;
        }

        public static CellFormat GetCellFormat(this WorkbookPart workbookPart, uint styleIndex)
        {
            return workbookPart.WorkbookStylesPart.Stylesheet.Elements<CellFormats>().First().Elements<CellFormat>().ElementAt((int)styleIndex);
        }

        public static CellFormat GetCellFormat(this Cell cell, WorkbookPart workbookPart)
        {
            return cell.StyleIndex != null
                ? workbookPart.GetCellFormat(cell.StyleIndex).CloneNode(true) as CellFormat
                : new CellFormat();
        }

        public static Worksheet MergeCells(this Worksheet worksheet, string cellName1, string cellName2)
        {
            MergeCells mergeCells;

            if (worksheet.Elements<MergeCells>().Any())
            {
                mergeCells = worksheet.Elements<MergeCells>().First();
            }
            else
            {
                mergeCells = new MergeCells();
                worksheet.InsertAfter(mergeCells, worksheet.Elements<SheetData>().First());
            }

            // Create the merged cell and append it to the MergeCells collection.
            var mergeCell = new MergeCell() { Reference = new StringValue(cellName1 + ":" + cellName2) };
            mergeCells.Append(mergeCell);

            return worksheet;
        }

        public static void MergeCells(this WorksheetPart worksheetPart, string cellName1, string cellName2)
        {
            worksheetPart.Worksheet.MergeCells(cellName1, cellName2);
        }

        public static string GetColumnName(int columnIndex)
        {
            var divedend = columnIndex;
            var columnName = string.Empty;
            while (divedend > 0)
            {
                var modifier = (divedend - 1) % 26;
                columnName = Convert.ToChar(65 + modifier) + columnName;
                divedend = (divedend - modifier) / 26;
            }

            return columnName;
        }

        public static uint GetRowIndex(string cellName)
        {
            Match match = RegNum.Match(cellName);

            return uint.Parse(match.Value);
        }

        public static string GetColumnName(string cellName)
        {
            Match match = RegChar.Match(cellName);

            return match.Value;
        }
    }
}
