﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Core.Utils.StyleSheet
{
    public static class CustomerBillDelivery
    {
        public const int startListBill = 15;
        public const int numFormatRow = 12;

        public static Dictionary<string, UInt32Value> BuildFormatIndex(SheetData sheetData, SheetData sheetFormat)
        {
            return new Dictionary<string, UInt32Value>
            {
                ["FieldNumOrder"] = sheetData.GetCell(1, startListBill).StyleIndex,
                ["FieldDate"] = sheetData.GetCell(2, startListBill).StyleIndex,
                ["FieldBillNo"] = sheetData.GetCell(3, startListBill).StyleIndex,
                ["FieldSenderAddress"] = sheetData.GetCell(4, startListBill).StyleIndex,
                ["FieldRecipientAddress"] = sheetData.GetCell(5, startListBill).StyleIndex,
                ["FieldNumberOfPackage"] = sheetData.GetCell(6, startListBill).StyleIndex,
                ["FieldWeightPackage"] = sheetData.GetCell(7, startListBill).StyleIndex,
                ["FieldPayment"] = sheetData.GetCell(8, startListBill).StyleIndex,
                ["FieldNote"] = sheetData.GetCell(9, startListBill).StyleIndex,
            };
        }

        public static void CopyTotalRows(ref SheetData sheetData, SheetData sheetFormat)
        {
            for (uint rowIndex = 1; rowIndex <= numFormatRow; rowIndex++)
            {
                Row formatRow = sheetFormat.GetRow(rowIndex);
                Row lastRow = sheetData.Elements<Row>().LastOrDefault();
                Row newRow = (Row)formatRow.Clone();
                newRow.RowIndex = lastRow.RowIndex + 1;

                // Update Row Reference
                newRow.Elements<Cell>().ToList()
                    .ForEach(c => c.CellReference = ExcelUtils.GetColumnName(c.CellReference) + newRow.RowIndex);

                sheetData.InsertAfter(newRow, lastRow);
            }
        }

        public static void CreateMergeCells(ref WorksheetPart worksheetPart, int startTotalRow)
        {
            worksheetPart.MergeCells("A" + (startTotalRow), "G" + (startTotalRow));
            worksheetPart.MergeCells("A" + (startTotalRow + 1), "G" + (startTotalRow + 1));
            worksheetPart.MergeCells("A" + (startTotalRow + 2), "G" + (startTotalRow + 2));

            worksheetPart.MergeCells("E" + (startTotalRow + 6), "I" + (startTotalRow + 6));
            worksheetPart.MergeCells("E" + (startTotalRow + 7), "I" + (startTotalRow + 7));
            worksheetPart.MergeCells("E" + (startTotalRow + 11), "I" + (startTotalRow + 11));
        }
    }
}
