﻿using System;
using Infrastructure.Core.Timing.Abstractions;

namespace Infrastructure.Core.Timing
{
    public class DotNetClockProvider : IClockProvider
    {
        public DateTime Now => DateTime.Now;

        public DateTimeKind Kind => DateTimeKind.Local;

        public DateTime? NormalizeNullable(DateTime? dateTime)
        {
            if (!dateTime.HasValue)
            {
                return null;
            }

            switch (dateTime.Value.Kind)
            {
                case DateTimeKind.Unspecified:
                    return DateTime.SpecifyKind(dateTime.Value, DateTimeKind.Local);
                case DateTimeKind.Utc:
                    return dateTime.Value.ToLocalTime();
                case DateTimeKind.Local:
                    return dateTime;
                default:
                    return dateTime;
            }
        }

        public DateTime Normalize(DateTime dateTime)
        {
            switch (dateTime.Kind)
            {
                case DateTimeKind.Unspecified:
                    return DateTime.SpecifyKind(dateTime, DateTimeKind.Local);
                case DateTimeKind.Utc:
                    return dateTime.ToLocalTime();
                case DateTimeKind.Local:
                    return dateTime;
                default:
                    return dateTime;
            }
        }

        public object NormalizeObject(object @object)
        {
            if (@object is DateTime d)
            {
                return Normalize(d);
            }

            return @object;
        }
    }
}
