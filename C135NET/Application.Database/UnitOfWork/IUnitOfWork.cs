using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Storage;

namespace Application.Database.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        object Insert(object o);

        object Update(object o);

        object Delete(object o);

        void InsertRange(IEnumerable<object> objects);

        void UpdateRange(IEnumerable<object> objects);

        void DeleteRange(IEnumerable<object> objects);

        IDbContextTransaction BeginTransaction();

        bool CommitSavePoint();

        void CommitTransaction(IDbContextTransaction transaction);

        void RollbackTransaction(IDbContextTransaction transaction);

        int Commit();
    }
}
