using System;
using System.Collections.Generic;
using System.Linq;
using Application.Database.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Storage;

namespace Application.Database.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private DbContext _dbContext;
        private bool _useTransaction;

        public UnitOfWork(IDesignTimeDbContextFactory<YamatoDbContext> contextFactory)
        {
            var dbContext = contextFactory.CreateDbContext(new string[0]);
            _dbContext = dbContext;
            _useTransaction = false;
        }

        public object Insert(object o)
        {
            _dbContext.Add(o);
            return o;
        }

        public object Update(object o)
        {
            _dbContext.Attach(o);
            _dbContext.Entry(o).State = EntityState.Modified;
            return o;
        }

        public object Delete(object o)
        {
            _dbContext.Remove(o);
            return o;
        }

        public void InsertRange(IEnumerable<object> objects)
        {
            _dbContext.AddRange(objects);
        }

        public void UpdateRange(IEnumerable<object> objects)
        {
            var list = objects.ToList();
            _dbContext.AttachRange(list);
            list.ForEach(o => _dbContext.Entry(o).State = EntityState.Modified);
        }

        public void DeleteRange(IEnumerable<object> objects)
        {
            _dbContext.RemoveRange(objects);
        }

        public IDbContextTransaction BeginTransaction()
        {
            _useTransaction = true;
            return _dbContext.Database.BeginTransaction();
        }

        public bool CommitSavePoint()
        {
            try
            {
                _dbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void CommitTransaction(IDbContextTransaction transaction)
        {
            transaction.Commit();
            _useTransaction = false;
        }

        public void RollbackTransaction(IDbContextTransaction transaction)
        {
            transaction.Rollback();
            _useTransaction = false;
        }

        public int Commit()
        {
            if (_useTransaction)
            {
                return 0;
            }
            // Save changes with the default options
            int result;

            using (var transaction = _dbContext.Database.BeginTransaction())
            {
                try
                {
                    result = _dbContext.SaveChanges();
                    transaction.Commit();
                }
                catch (DbUpdateConcurrencyException)
                {
                    transaction.Rollback();
                    throw;
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    throw new DbUpdateException("There is an error when commit transaction.", e);
                }
            }
            return result;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposing || _dbContext == null)
            {
                return;
            }

            _dbContext.Dispose();
            _dbContext = null;
        }
    }
}
