using Application.Database.Entities.Auditing;
using Infrastructure.Core.Timing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Linq;

namespace Application.Database.EntityFrameworkCore
{
    public class YamatoDbContextBase : DbContext
    {
        public YamatoDbContextBase(DbContextOptions options) : base(options)
        {
        }

        public override int SaveChanges()
        {
            ApplyDatabaseConcepts();
            return base.SaveChanges();
        }

        private void ApplyDatabaseConcepts()
        {
            var entries = ChangeTracker.Entries().ToList();
            foreach (var entry in entries)
            {
                SetLastEditedByProperties(entry.Entity);

                switch (entry.State)
                {
                    case EntityState.Added:
                        SetCreationAuditProperties(entry.Entity);
                        break;

                    case EntityState.Modified:
                        SetModificationAuditProperties(entry.Entity);
                        break;

                    case EntityState.Deleted:
                        if (IsSoftDelete(entry.Entity))
                        {
                            CancelDeletionForSoftDelete(entry);
                            SetDeletionAuditProperties(entry.Entity);
                        }

                        break;

                    case EntityState.Detached:
                        break;

                    case EntityState.Unchanged:
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private static bool IsSoftDelete(object entityAsObj)
        {
            return entityAsObj is ISoftDelete;
        }

        private static void CancelDeletionForSoftDelete(EntityEntry entry)
        {
            if (!(entry.Entity is ISoftDelete)) { return; }

            entry.State = EntityState.Unchanged;
        }

        private static void SetDeletionAuditProperties(object entityAsObj)
        {
            if (!(entityAsObj is ISoftDelete entityWithSoftDelete)) { return; }

            entityWithSoftDelete.IsDeleted = true;
        }

        private static void SetCreationAuditProperties(object entityAsObj)
        {
            if (!(entityAsObj is IHasCreationTime entityWithCreationTime)) { return; }

            entityWithCreationTime.CreateTime = Clock.Now;
        }

        private static void SetModificationAuditProperties(object entityAsObj)
        {
            if (!(entityAsObj is IHasModificationTime entityWithModificationTime)) { return; }

            entityWithModificationTime.LastModifyTime = Clock.Now;
        }

        private static void SetLastEditedByProperties(object entityAsObj)
        {
            if (!(entityAsObj is IHasLastEditedBy entityWithLastEditedBy)) { return; }

            entityWithLastEditedBy.LastEditedBy = null;
        }
    }
}
