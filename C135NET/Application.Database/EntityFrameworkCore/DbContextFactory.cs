using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Application.Database.EntityFrameworkCore
{
    public class DbContextFactory : IDesignTimeDbContextFactory<YamatoDbContext>
    {
        public YamatoDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<YamatoDbContext>();

            var configuration = Configuration.AppConfigurations.Get(Configuration.WebContentDirectoryFinder.CalculateContentRootFolder());

            DbContextConfigure.Configure(builder, configuration.GetConnectionString("DefaultConnection"));

            return new YamatoDbContext(builder.Options);
        }
    }
}
