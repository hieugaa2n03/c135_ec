using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace Application.Database.EntityFrameworkCore
{
    public static class DbContextConfigure
    {
        public static void Configure(DbContextOptionsBuilder<YamatoDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<YamatoDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
