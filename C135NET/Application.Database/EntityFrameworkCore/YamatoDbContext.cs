using System;
using System.Linq.Expressions;
using System.Reflection;
using Application.Database.Entities;
using Application.Database.Entities.Auditing;
using Infrastructure.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Application.Database.EntityFrameworkCore
{
    public class YamatoDbContext : YamatoDbContextBase
    {
        private static readonly MethodInfo ConfigureGlobalFiltersMethodInfo =
            typeof(YamatoDbContext).GetMethod(nameof(ConfigureGlobalFilters),
                BindingFlags.Instance | BindingFlags.NonPublic);

        protected virtual bool IsSoftDeleteFilterEnabled => true;

        //Database

        public DbSet<Driver> Drivers { get; set; }

        public DbSet<SaleAgent> SaleAgents { get; set; }

        public DbSet<Customer> Customers { get; set; }
    

        public YamatoDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            foreach (var entityType in modelBuilder.Model.GetEntityTypes())
            {
                ConfigureGlobalFiltersMethodInfo
                    .MakeGenericMethod(entityType.ClrType)
                    .Invoke(this, new object[] { modelBuilder, entityType });
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.ReplaceService<IEntityMaterializerSource, AppEntityMaterializeSource>();

            base.OnConfiguring(optionsBuilder);
        }

        protected void ConfigureGlobalFilters<TEntity>(ModelBuilder modelBuilder, IMutableEntityType entityType) where TEntity : class
        {
            if (entityType.BaseType != null || !ShouldFilterEntity<TEntity>(entityType))
            {
                return;
            }

            var filterExpression = CreateFilterExpression<TEntity>();
            if (filterExpression != null)
            {
                modelBuilder.Entity<TEntity>().HasQueryFilter(filterExpression);
            }
        }

        protected virtual bool ShouldFilterEntity<TEntity>(IMutableEntityType entityType) where TEntity : class
            => typeof(ISoftDelete).IsAssignableFrom(typeof(TEntity));

        protected virtual Expression<Func<TEntity, bool>> CreateFilterExpression<TEntity>() where TEntity : class
        {
            if (!typeof(ISoftDelete).IsAssignableFrom(typeof(TEntity)))
            {
                return null;
            }

            Expression<Func<TEntity, bool>> softDeleteFilter = e => !((ISoftDelete)e).IsDeleted || ((ISoftDelete)e).IsDeleted != IsSoftDeleteFilterEnabled;
            var expression = softDeleteFilter;

            return expression;
        }

        protected virtual Expression<Func<T, bool>> CombineExpressions<T>(Expression<Func<T, bool>> expression1, Expression<Func<T, bool>> expression2)
        {
            var parameter = Expression.Parameter(typeof(T));

            var leftVisitor = new ReplaceExpressionVisitor(expression1.Parameters[0], parameter);
            var left = leftVisitor.Visit(expression1.Body);

            var rightVisitor = new ReplaceExpressionVisitor(expression2.Parameters[0], parameter);
            var right = rightVisitor.Visit(expression2.Body);

            if (left == null || right == null)
            {
                throw new ArgumentNullException();
            }

            return Expression.Lambda<Func<T, bool>>(Expression.AndAlso(left, right), parameter);
        }
    }
}
