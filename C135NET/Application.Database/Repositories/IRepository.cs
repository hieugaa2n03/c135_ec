using System;

namespace Application.Database.Repositories
{
    public interface IRepository<TEntity> : IRepository<TEntity, Guid> where TEntity : class
    {
    }
}
