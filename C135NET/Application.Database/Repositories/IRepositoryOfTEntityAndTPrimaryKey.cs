using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Application.Database.Repositories
{
    public interface IRepository<TEntity, in TPrimaryKey> where TEntity : class
    {
        TEntity GetById(TPrimaryKey id);

        TEntity Get(Expression<Func<TEntity, bool>> where);

        IQueryable<TEntity> GetAll();

        IQueryable<TEntity> GetMany(Expression<Func<TEntity, bool>> where);

        TEntity Insert(TEntity entity);

        TEntity Update(TEntity entity);

        TEntity Delete(TEntity entity);

        void UpdateRange(IEnumerable<TEntity> entities);

        void InsertRange(IEnumerable<TEntity> entities);

        void DeleteRange(IEnumerable<TEntity> entities);

        List<TEntity> Delete(Expression<Func<TEntity, bool>> where);

        void Save();
    }
}
