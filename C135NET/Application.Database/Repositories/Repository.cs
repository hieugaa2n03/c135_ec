using System;
using Application.Database.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Application.Database.Repositories
{
    public class Repository<TEntity> : Repository<TEntity, Guid>, IRepository<TEntity> where TEntity : class
    {
        public Repository(IDesignTimeDbContextFactory<YamatoDbContext> dbContextFactory) : base(dbContextFactory)
        {

        }
    }
}
