using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Application.Database.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Application.Database.Repositories
{
    public class Repository<TEntity, TPrimary> : IRepository<TEntity, TPrimary> where TEntity : class
    {
        protected readonly YamatoDbContext Context;

        protected readonly DbSet<TEntity> Table;

        public Repository(IDesignTimeDbContextFactory<YamatoDbContext> dbContextFactory)
        {
            Context = dbContextFactory.CreateDbContext(new string[0]);
            Table = Context.Set<TEntity>();
        }

        public virtual TEntity GetById(TPrimary id)
        {
            return Table.Find(id);
        }

        public virtual TEntity Get(Expression<Func<TEntity, bool>> where)
        {
            var entity = Table.Where(where).FirstOrDefault();
            return entity;
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            return Table;
        }

        public virtual IQueryable<TEntity> GetMany(Expression<Func<TEntity, bool>> where)
        {
            return Table.Where(where);
        }

        public virtual TEntity Insert(TEntity entity)
        {
            Table.Add(entity);
            Save();

            return entity;
        }

        public virtual TEntity Update(TEntity entity)
        {
            Table.Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
            Save();

            return entity;
        }

        public virtual TEntity Delete(TEntity entity)
        {
            Table.Remove(entity);
            Save();

            return entity;
        }

        public virtual void InsertRange(IEnumerable<TEntity> entities)
        {
            Table.AddRange(entities);
            Save();
        }

        public virtual void UpdateRange(IEnumerable<TEntity> entities)
        {
            var list = entities.ToList();
            Table.AttachRange(list);
            list.ForEach(o => Context.Entry(list).State = EntityState.Modified);
            Save();
        }

        public virtual void DeleteRange(IEnumerable<TEntity> entities)
        {
            Table.RemoveRange(entities);
            Save();
        }

        public virtual List<TEntity> Delete(Expression<Func<TEntity, bool>> where)
        {
            var objects = Table.Where(where).AsEnumerable();
            var deletedObjects = objects.Select(Delete).ToList();

            return deletedObjects;
        }

        public virtual void Save()
        {
            Context.SaveChanges();
        }
    }
}
