﻿using Application.Database.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Application.Database.Entities
{
    public class Driver : FullAuditedEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(8)]
        public string DriverCode { get; set; }

        [Required]
        [MaxLength(255)]
        public string DriverName { get; set; }

        [MaxLength(11)]
        public string DriverContactTel { get; set; }

    }
}
