﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Database.Entities.Auditing
{
    public interface IHasDeletionUser
    {
        string DeletionUser { get; set; }
    }
}
