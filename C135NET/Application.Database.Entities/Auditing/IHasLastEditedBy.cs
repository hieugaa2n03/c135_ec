namespace Application.Database.Entities.Auditing
{
    public interface IHasLastEditedBy
    {
        string LastEditedBy { get; set; }    
    }
}
