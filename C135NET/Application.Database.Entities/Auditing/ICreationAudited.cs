namespace Application.Database.Entities.Auditing
{
    public interface ICreationAudited : IHasCreationTime
    {
    }
}
