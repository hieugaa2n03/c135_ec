using System;

namespace Application.Database.Entities.Auditing
{
    public abstract class AuditedEntity : IAudited
    {
        public DateTime CreateTime { get; set; }

        public DateTime? LastModifyTime { get; set; }
    }
}
