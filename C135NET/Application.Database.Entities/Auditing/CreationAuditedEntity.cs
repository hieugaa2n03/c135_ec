using System;

namespace Application.Database.Entities.Auditing
{
    public abstract class CreationAuditedEntity : ICreationAudited
    {
        public DateTime CreateTime { get; set; }
    }
}
