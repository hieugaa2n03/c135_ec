namespace Application.Database.Entities.Auditing
{
    public interface IFullAudited : IAudited, IHasDeletionTime, ISoftDelete, IHasLastEditedBy
    {
    }
}
