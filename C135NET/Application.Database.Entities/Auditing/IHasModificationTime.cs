using System;

namespace Application.Database.Entities.Auditing
{
    public interface IHasModificationTime
    {
        DateTime? LastModifyTime { get; set; }
    }
}
