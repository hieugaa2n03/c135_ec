using System;

namespace Application.Database.Entities.Auditing
{
    public abstract class FullAuditedEntity : IFullAudited
    {
        public string CreateBy { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime? LastModifyTime { get; set; }

        public DateTime? DeleteTime { get; set; }

        public string LastEditedBy { get; set; }

        public bool IsDeleted { get; set; }
    }
}
