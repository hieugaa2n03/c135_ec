namespace Application.Database.Entities.Auditing
{
    public interface IAudited : IHasCreationTime, IHasModificationTime
    {
    }
}
