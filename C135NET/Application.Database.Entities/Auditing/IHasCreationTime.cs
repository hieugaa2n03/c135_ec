using System;

namespace Application.Database.Entities.Auditing
{
    public interface IHasCreationTime
    {
        DateTime CreateTime { get; set; }
    }
}
