﻿namespace Application.Database.Entities.Auditing
{
    public interface IHasCreationUser
    {
        string CreationUser { get; set; }
    }
}
