using System;

namespace Application.Database.Entities.Auditing
{
    public interface IHasDeletionTime
    {
        DateTime? DeleteTime { get; set; }
    }
}
