﻿using Application.Database.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Application.Database.Entities
{
    public class Customer : FullAuditedEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(8)]
        public string CustomerCode { get; set; }

        public string CustomerName { get; set; }

        public string CustomerContactName { get; set; }

        [Required]
        [MaxLength(255)]
        public string CustomerContactTel { get; set; }

        public Guid SaleAgentId { get; set; }

        [Required]
        [MaxLength(500)]
        public string Address { get; set; }

        public SaleAgent SaleAgent { get; set; }
    }
}
